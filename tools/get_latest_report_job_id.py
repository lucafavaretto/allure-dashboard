# SPDX-License-Identifier: Apache-2.0
#
# SPDX-FileCopyrightText: Kalpa srl

import json
import sys
from urllib.request import urlopen


def get(url, object_hook=None):
    with urlopen(url) as resource:  # 'with' is important to close the resource after use
        return json.load(resource, object_hook=object_hook)


jobs = get("https://gitlab.eclipse.org/api/v4/projects/3126/jobs?scope[]=success&private_token=%s"% sys.argv[1])
found = None
for job in jobs:
    if job.get("name") == "report":
        found = job
        break

print(found.get('id'))
