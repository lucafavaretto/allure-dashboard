# SPDX-License-Identifier: Apache-2.0
#
# SPDX-FileCopyrightText: Kalpa srl

import json
import logging
import re
from datetime import date
from os import makedirs, path
from urllib.request import urlopen


def fix_junit_content(content, board_name):
    content = content.replace(' name="0_basic-smoke-tests"',  ' name="Basic Smoke Tests"')
    content = content.replace(' name="0_perf"',               ' name="Performance Tests"')
    content = content.replace(' name="0_zephyr-tests"',       ' name="Zephyr Tests"')
    content = content.replace(' name="0_ptest-podman"',       ' name="Container Tests (podman)"')
    content = content.replace(' name="1_kselftest-rtc"',      ' name="Kernel Self Tests RTC"')
    content = content.replace(' name="2_ptest-libc-test"',    ' name="LibC Tests"')
    content = content.replace(' name="lava"',                 ' name="Lava Tests"')
    content = content.replace(' name="0_cpu"',                 ' name="Checkbox Tests"')

    #TODO: in the future use RegExp to detect all "1_ltp-" tests and replace them
    #import re 
    #content = re.sub('(\"1_lpt\-[a-z\-]+\")', 'LPT Tests ', content)

    content = content.replace('"1_ltp-cap_bounds-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-commands-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-containers-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-controllers-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-cpuhotplug-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-crypto-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-cve-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-dio-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-filecaps-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-fcntl-locktests-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-fs_bind-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-fs_perms_simple-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-fs-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-fsx-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-hugetlb-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-io-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-ipc-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-math-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-mm-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-nptl-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-pty-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-sched-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-securebits-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-syscalls-tests"', '"LTP Tests"')
    content = content.replace('"1_ltp-tracing-tests"', '"LTP Tests"')

    content = re.sub(r"<testcase name=\"([a-zA-Z\-\(\)\_\s\d]+)\"", r'<testcase name="\1-%s"' % board_name, content)

    return content


def get_junit(url, id, folder, board_name):
    junit_content = urlopen(url).read().decode('utf-8')
    junit_file = "%s/%s.xml" % (folder, id)
    junit_content = fix_junit_content(junit_content, board_name)
    f = open(junit_file, 'w')
    f.write(junit_content)
    f.close()


def get(url, object_hook=None):
    with urlopen(url) as resource:  # 'with' is important to close the resource after use
        return json.load(resource, object_hook=object_hook)


def get_builds(project_id):
    return get("https://squadp.svc.ostc-eu.dev/api/projects/%s/builds/" % project_id).get("results")


def get_latest_finished_build(project_id):
    builds = get_builds(project_id=project_id)
    for build in builds:
        if build.get("finished") is True:
            return build


def get_lava_job_ids(build_id):
    url = "https://squadp.svc.ostc-eu.dev/api/builds/%s/testjobs?limit=10000" % build_id
    jobs = get(url).get("results")
    ids = []
    for job in jobs:
        if job.get("external_url") is not None:
            ids.append({'id': job.get("external_url").split("/")[-1], 'env': job.get('environment')})
    return ids


def is_new_build(id):
    try:
        with open('latest_build.json') as json_file:
            data = json.load(json_file)
            if data['build_id'] == id:
                return False
    except FileNotFoundError:
        # If not file is found, no history is found, so the build can be considered as new
        return True
    except KeyError:
        # A file with invalid JSON is returned on the first run        
        return True
    return True


def create_folder_for_environment(project_name, environment, build_id, version, full_name, build_order):
    folder_name = 'allure-results-%s-%s' % (environment, project_name)
    if not path.isdir(folder_name):
        makedirs(folder_name, exist_ok=True)
        executor_data = {
            'name': environment,
            'type': 'lava',
            'url': 'https://squadp.svc.ostc-eu.dev/%s/' % full_name,
            'buildOrder': build_order,
            'buildName': "%s/%s/%s" % (environment, project_name, build_id),
            'buildUrl': "https://squadp.svc.ostc-eu.dev/%s/build/%s/testjobs/?environment=%s" % (full_name, version, environment),
            'reportUrl': "https://squadp.svc.ostc-eu.dev/%s/build/%s/testjobs/?environment=%s" % (full_name, version, environment),
            'reportName': "%s/%s/%s" % (environment, project_name, build_id)
        }
        with open(path.join(folder_name, 'executor.json'), 'w') as outfile:
            json.dump(executor_data, outfile, indent=2)
    return folder_name


## Start

#logging.basicConfig(filename='fetch_lava_junit_reports.log', encoding='utf-8', level=logging.DEBUG)
logging.basicConfig(level=logging.INFO)

build_order = (date.today() - date(2020, 1, 1)).days
logging.info("Fetching data for build %d" % build_order)

# Search on the Lava node for all the projects that contains the string "scheduled-" in the full name
all_projects = get("https://squadp.svc.ostc-eu.dev/api/projects/?limit=1000").get("results")
projects = []
for project in all_projects:
    if "scheduled-" in project.get('full_name'):
        logging.debug("Adding project %s to pipeline" % project.get('full_name'))
        projects.append(project)
logging.info("%d scheduled projects found" % len(projects))

# Get the jUnit files for every project
for project in projects:
    logging.info("Fetching data for project %s" % project.get('name'))
    build = get_latest_finished_build(project_id=project.get('id'))
    try:
        build_id = build.get("id")
    except AttributeError:
        logging.warning("Finished build not found for project %s" % project.get('name'))
        continue
    logging.info("Latest finished build - id: %s - version: %s" % (build_id, build.get("version")))
    version = build.get("version")

    #if is_new_build(id=build_id) is True:
    #    print("Latest build is %s" % build_id)
    #    # open('new_build.txt', mode='w').close()
    #else:
    #    print("Latest build is %s and has already been imported. Skipping." % build_id)
    #    exit(1)  # Let the job stop and stop the pipeline

    #build_data = {
    #    'build_id': build_id
    #}

    #with open('latest_build.json', 'w') as outfile:
    #    outfile.write(json.dumps(build_data))

    ids = get_lava_job_ids(build_id=build_id)
    logging.info("%d jobs found in project %s" % (len(ids), project.get('name')))

    for id in ids:
        url = "https://lava.ostc-eu.org/api/v0.2/jobs/%s/junit" % id.get('id')
        logging.debug(url)
        env = id.get('env')
        folder = create_folder_for_environment(project.get('name'), env, build_id, version, project.get('full_name'), build_order)
        get_junit(url=url, id=id.get('id'), folder=folder, board_name=env)
