# SPDX-FileCopyrightText: Huawei Inc.
#
# SPDX-License-Identifier: Apache-2.0

#!/bin/bash

function make_report {
    allure/bin/allure generate --report-dir allure-reports/$1 --clean --configDirectory config allure-results-$1-*
    mv allure-results-$1-* used_reports
}

mkdir -p used_reports
make_report qemu-cortex
make_report qemu-x86
make_report qemux86-64
make_report qemux86
make_report qemuarm64
make_report qemuarm
make_report raspberrypi4-64
make_report seco-imx8mm-c61-2gb
make_report seco-imx8mm-c61-4gb
make_report seco-intel-b68
mv used_reports/* .
rmdir used_reports
